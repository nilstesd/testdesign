Egenskap: Teste ekvivalensklasser og grenseverdier
Som ivrig turist
Ønsker jeg å kjøpe biletter online for å spare tid

Scenariomal: Kjøpe billetter
Gitt at jeg skal kjøpe flere billetter på billetter.no
Når jeg legger inn <antall> billetter og velger kjøp
Så skal prisen bli <pris>

Eksempler: Ekvivalensklasser
   | antall | pris                                         | 
   | -12    | Feil. Antall må være 0 eller større.         | 
   | 3      | 360                                          | 
   | 14     | 1680                                         | 
   | 110    | Feil. Du kan maks kjøpe 100 billetter.       | 
   
Eksempler: Grenseverdier
   | antall | pris                                         |
   | 0      | Feil. Det er ingen billetter i handlekurven. |
   | 1      | 120                                          | 
   | 2      | 240                                          | 
   | 9      | 1080                                         | 
   | 10     | 1200                                         | 
   | 11     | 1320                                         | 
   | 99     | 11880                                        | 
   | 100    | 12000                                        | 
   | 101    | Feil. Du kan maks kjøpe 100 billetter.       | 
   
Scenariomal: Kjøpe billetter
Gitt at jeg skal kjøpe flere billetter på billetter.no
Når jeg legger inn <voksne> voksne og <barn> barn og velger <familie> og trykker kjøp
Så skal prisen bli <pris>

Eksempler: Beslutningstabell
| voksne | barn | familie | pris |
| 2      | 2    | True    | 240  |
| 2      | 2    | False   | 360  |
| 1      | 2    | True    | 216  |
| 1      | 2    | False   | 240  |