from behave import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
import os

@given(u'at jeg skal kjøpe flere billetter på billetter.no')
def step_impl(context):
    context.driver = webdriver.Chrome()    
    context.driver.get(os.getcwd() + "/billetter.html")
    
@when(u'jeg legger inn {antall} billetter og velger kjøp')
def step_impl(context, antall):
    elem = context.driver.find_element_by_id("adults")
    elem.clear();
    elem.send_keys(antall)
    elem = context.driver.find_element_by_id("buy")
    elem.click()

@when(u'jeg legger inn {voksne} voksne og {barn} barn og velger {familie} og trykker kjøp')
def step_impl(context, voksne, barn, familie):
    elem = context.driver.find_element_by_id("adults")
    elem.clear()
    elem.send_keys(voksne)
    elem = context.driver.find_element_by_id("children")
    elem.clear()
    elem.send_keys(barn)
    elem = context.driver.find_element_by_id("family")
    if familie == "True":
        elem.click()
    
    elem = context.driver.find_element_by_id("buy")
    elem.click()

@then(u'skal prisen bli {pris}')
def step_impl(context, pris):
    element = context.driver.find_element_by_id("price")
    forventetPris = element.text
    print("Pris: " + pris)
    print("Forventet pris: " + forventetPris)
    assert (pris == forventetPris)
    context.driver.close()