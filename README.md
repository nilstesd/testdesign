# Testdesign med Behave og Selenium

Husk at Behave og Selenium må være installert.

Last ned prosjektet. Endre evt chrome options slik at Selenium finner chromedriver.exe. 
Kjør så med kommandoen:

```
behave --lang=no
```

Resultatet av kjøringen bør bli:

```
1 feature passed, 0 failed, 0 skipped
17 scenarios passed, 0 failed, 0 skipped
51 steps passed, 0 failed, 0 skipped, 0 undefined
```
Du kan også teste billetter.html direkte ved å åpne den i en nettleser.